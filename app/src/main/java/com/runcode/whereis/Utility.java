package com.runcode.whereis;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by user on 28/7/2016.
 */
public class Utility {

    public static final int REQUEST_CHECK_SETTINGS = 0x1;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    public final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    public final static String KEY_LOCATION = "location";
    public final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";


    public static void displayShortToast(Context c, String msg){
        Toast.makeText(c, msg, Toast.LENGTH_LONG).show();
    }
}
